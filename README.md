# Instalation #

To run service run in cli: 

```
docker-compose -f docker-compose.local.yml up --build
```

# Setup #

.env file:

```
DB_Addr=localhost:6379
DB_Pass=sOmE_sEcUrE_pAsS
DB_Name=0
JWTSecret=3j2kb42bfhbwdjhw32
```

# Endpoints #

### GET ###
```
http://[::]:3000/v1/secret/hash
```

![HTTP_GET_200](resources/get200.png)
![HTTP_GET_404](resources/get404.png)

### POST ###
```
http://[::]:3000/v1/secret?secret=fsdfsdfsdfdsfdsf&expireAfter=120&expireAfterViews=30
```
![HTTP_GET_200](resources/post.png)


 