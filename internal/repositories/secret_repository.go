package repositories

import (
	"swaggerrestapi/api/models"
	"swaggerrestapi/clients"
	"time"
)

type SecretHandler interface {
	FetchSecretByHash(db *clients.Client, hash string) (*models.Secret, error)
	CreateNewSecret(db *clients.Client, secret *models.Secret, expire time.Duration) error
	Delete(db *clients.Client, hash string) error
}

type SecretRepository struct {}

func ProvideSecretRepository() SecretHandler {
	return &SecretRepository{}
}

func (s *SecretRepository) FetchSecretByHash(db *clients.Client, hash string) (*models.Secret, error){
	client := *db;
	val, err := client.Get(hash)
	if err != nil {
		return nil, err
	}

	model := models.Secret{};
	err2 := model.UnmarshalBinary([]byte(val))
	if err2 != nil {
		return nil, err2
	}

	return &model, nil
}

func (s *SecretRepository) CreateNewSecret(db *clients.Client, secret *models.Secret, expire time.Duration) error {
	client := *db;
	bin, err := secret.MarshalBinary()
	if err != nil {
		return err
	}

	err2 := client.Add(secret.Hash, bin,expire)
	if err2 != nil {
		return err2
	}

	return nil
}

func (s *SecretRepository) Delete(db *clients.Client, hash string) error {
	client := *db;
	err := client.Delete(hash)
	if err != nil {
		return err
	}
	return nil
}