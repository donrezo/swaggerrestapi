module swaggerrestapi

go 1.15

require (
	github.com/go-openapi/errors v0.19.9
	github.com/go-openapi/loads v0.20.0
	github.com/go-openapi/runtime v0.19.24
	github.com/go-openapi/spec v0.20.1
	github.com/go-openapi/strfmt v0.20.0
	github.com/go-openapi/swag v0.19.13
	github.com/go-openapi/validate v0.20.1
	github.com/go-redis/redis/v8 v8.4.8
	github.com/jessevdk/go-flags v1.4.0
	github.com/mitchellh/mapstructure v1.4.1 // indirect
	github.com/sakirsensoy/genv v1.0.1
	github.com/supanadit/jwt-go v1.3.1
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	golang.org/x/text v0.3.5 // indirect
)
