package configs

import "github.com/sakirsensoy/genv"

type appConfig struct {
	DBAddr string
	DBPass string
	DBName int
	JWTSecret string
}

var  App = &appConfig{
	DBAddr: genv.Key("DB_Address").String(),
	DBPass: genv.Key("DB_Pass").String(),
	DBName: genv.Key("DB_Name").Int(),
	JWTSecret: genv.Key("JWTSecret").String(),
}
