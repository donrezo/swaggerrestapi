package clients

import (
	"context"
	"github.com/go-redis/redis/v8"
	"time"
)

var ctx = context.Background()

type Client interface {
	Get(key string) (string, error)
	Add(key string, data []byte, expirationTime time.Duration) error
	Delete(key string) error
}

type RedisClient struct {
	rdb *redis.Client
}

func ProvideNewRedisClient(rdb *redis.Client) Client {
	return RedisClient{
		rdb: rdb,
	}
}

func (r RedisClient) Get(key string) (string, error){
	val, err := r.rdb.Get(ctx, key).Result()
	if err != nil {
		return "", err
	}

	return val, nil
}

func (r RedisClient) Add(key string, data []byte, expirationTime time.Duration) error{
	err := r.rdb.Set(ctx, key, data,expirationTime).Err()
	if err != nil {
		return err
	}

	return nil
}

func (r RedisClient) Delete(key string) error{
	err := r.rdb.Del(ctx, key).Err()
	if err != nil {
		return err
	}
	return nil
}

func (r RedisClient) UpdateUseCounter(key string) error{
	err := r.rdb.Del(ctx, key).Err()
	if err != nil {
		return err
	}
	return nil
}

