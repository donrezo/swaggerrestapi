// This file is safe to edit. Once it exists it will not be overwritten

package restapi

import (
	_ "github.com/sakirsensoy/genv/dotenv/autoload"
	"crypto/tls"
	"net/http"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/runtime"
	"github.com/go-openapi/runtime/middleware"

	"swaggerrestapi/api/restapi/operations"
	"swaggerrestapi/api/restapi/operations/secret"
	"github.com/go-redis/redis/v8"
	"swaggerrestapi/handlers"
	"swaggerrestapi/internal/repositories"
	"swaggerrestapi/clients"

	"swaggerrestapi/configs"
)

//go:generate swagger generate server --target ../../gen --name Secret --spec ../../swagger/swagger.yml --principal interface{} --exclude-main

func configureFlags(api *operations.SecretAPI) {
	// api.CommandLineOptionsGroups = []swag.CommandLineOptionsGroup{ ... }
}

func configureAPI(api *operations.SecretAPI) http.Handler {
	api.ServeError = errors.ServeError

	rdb := redis.NewClient(&redis.Options{
		Addr:     configs.App.DBAddr,
		Password: configs.App.DBPass,
		DB:       configs.App.DBName,
	})

	redisClient := clients.ProvideNewRedisClient(rdb)
	repo := repositories.ProvideSecretRepository()

	api.UseSwaggerUI()
	api.SecretAddSecretHandler = handlers.NewAddSecretHandler(&redisClient,repo)
	api.SecretGetSecretByHashHandler = handlers.NewGetSecretHandler(&redisClient,repo)

	api.UrlformConsumer = runtime.DiscardConsumer

	api.JSONProducer = runtime.JSONProducer()
	api.XMLProducer = runtime.XMLProducer()


	if api.SecretAddSecretHandler == nil {
		api.SecretAddSecretHandler = secret.AddSecretHandlerFunc(func(params secret.AddSecretParams) middleware.Responder {
			return middleware.NotImplemented("operation secret.AddSecret has not yet been implemented")
		})
	}
	if api.SecretGetSecretByHashHandler == nil {
		api.SecretGetSecretByHashHandler = secret.GetSecretByHashHandlerFunc(func(params secret.GetSecretByHashParams) middleware.Responder {
			return middleware.NotImplemented("operation secret.GetSecretByHash has not yet been implemented")
		})
	}

	api.PreServerShutdown = func() {}

	api.ServerShutdown = func() {}

	return setupGlobalMiddleware(api.Serve(setupMiddlewares))
}

// The TLS configuration before HTTPS server starts.
func configureTLS(tlsConfig *tls.Config) {
	// Make all necessary changes to the TLS configuration here.
}

// As soon as server is initialized but not run yet, this function will be called.
// If you need to modify a config, store server instance to stop it individually later, this is the place.
// This function can be called multiple times, depending on the number of serving schemes.
// scheme value will be set accordingly: "http", "https" or "unix"
func configureServer(s *http.Server, scheme, addr string) {
}

// The middleware configuration is for the handler executors. These do not apply to the swagger.json document.
// The middleware executes after routing but before authentication, binding and validation
func setupMiddlewares(handler http.Handler) http.Handler {
	return handler
}

// The middleware configuration happens before anything, this middleware also applies to serving the swagger.json document.
// So this is a good place to plug in a panic handling middleware, logging and metrics
func setupGlobalMiddleware(handler http.Handler) http.Handler {
	return handler
}
