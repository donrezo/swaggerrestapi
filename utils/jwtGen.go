package utils

import (
	_ "github.com/sakirsensoy/genv/dotenv/autoload"
	"github.com/supanadit/jwt-go"
	"swaggerrestapi/configs"
)

func GenerateKeyForSecret(hash string) (string, error){
	jwt.SetJWTSecretCode(configs.App.JWTSecret)
	token, err := jwt.GenerateJWT(hash)
	if err != nil {
		return "",err
	}
	return token, nil
}
