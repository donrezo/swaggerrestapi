package main

import (
	"flag"
	"log"
	"github.com/go-openapi/loads"
	"swaggerrestapi/api/restapi"
	"swaggerrestapi/api/restapi/operations"
)

var portFlag = flag.Int("port", 3000, "Port to run this service on")

func main()  {
	swaggerSpec, err := loads.Analyzed(restapi.SwaggerJSON, "")
	if err != nil {
		log.Fatalln(err)
	}

	api := operations.NewSecretAPI(swaggerSpec)
	server := restapi.NewServer(api)
	defer server.Shutdown()
	flag.Parse()
	server.Port = *portFlag
	server.ConfigureAPI()

	if err := server.Serve(); err != nil {
		log.Fatalln(err)
	}
}
