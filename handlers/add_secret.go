package handlers

import (
	"swaggerrestapi/api/restapi/operations/secret"
	"github.com/go-openapi/runtime/middleware"
	"swaggerrestapi/internal/repositories"
	"swaggerrestapi/clients"
	"swaggerrestapi/api/models"
	"time"
	"github.com/go-openapi/strfmt"
	"swaggerrestapi/utils"
	"fmt"
)

type addSecret struct {
	redisClient *clients.Client
	secretRepository repositories.SecretHandler
}

func NewAddSecretHandler(rds *clients.Client, repo repositories.SecretHandler) secret.AddSecretHandler {
	return &addSecret{
		redisClient: rds,
		secretRepository: repo,
	}
}

func (a *addSecret) Handle(params secret.AddSecretParams) middleware.Responder {

	if (params.ExpireAfter == 0 && params.ExpireAfterViews == 0 && params.Secret == ""){
		return secret.NewAddSecretMethodNotAllowed()
	}

	token, err := utils.GenerateKeyForSecret(params.Secret)
	if err != nil {
		//log err
		secret.NewAddSecretMethodNotAllowed()
	}

	model, val := createObjectByParams(params, token)

	err2 := a.secretRepository.CreateNewSecret(a.redisClient, &model, val)
	if err2 != nil {
		fmt.Println(err2)
		secret.NewAddSecretMethodNotAllowed()
	}

	return secret.NewAddSecretOK().WithPayload(&model)
}

func createObjectByParams(params secret.AddSecretParams, token string) (models.Secret, time.Duration) {
	model := models.Secret{}
	model.SecretText = params.Secret

	val := time.Duration(params.ExpireAfter) * time.Second
	date := time.Now()
	cat := strfmt.DateTime(date);
	date = date.Add(val);
	datetime := strfmt.DateTime(date);

	model.CreatedAt = cat
	model.Hash = token
	model.ExpiresAt = datetime
	model.RemainingViews = params.ExpireAfterViews
	model.SecretText = params.Secret

	return model, val
}
