package handlers

import (
	"swaggerrestapi/api/restapi/operations/secret"
	"github.com/go-openapi/runtime/middleware"
	"swaggerrestapi/internal/repositories"
	"swaggerrestapi/clients"
	"swaggerrestapi/api/models"
	"time"
)

type getSecret struct {
	redisClient *clients.Client
	secretRepository repositories.SecretHandler
}

func NewGetSecretHandler(rds *clients.Client, repo repositories.SecretHandler) secret.GetSecretByHashHandler {
	return &getSecret{
		redisClient: rds,
		secretRepository: repo,
	}
}

func (g *getSecret) Handle(params secret.GetSecretByHashParams) middleware.Responder {
	val, err := g.secretRepository.FetchSecretByHash(g.redisClient,params.Hash)
	if err != nil {
		return secret.NewGetSecretByHashNotFound()
	}

	if val.RemainingViews <= 0 {
		go g.secretRepository.Delete(g.redisClient,params.Hash)
		return secret.NewGetSecretByHashNotFound()
	}

	go g.updateCounter(params.Hash, val)

	return secret.NewGetSecretByHashOK().WithPayload(val)
}

func (g *getSecret) updateCounter(key string, secret *models.Secret) {
	err := g.secretRepository.Delete(g.redisClient,key)
	if err != nil {
		//log err
		return
	}

	secret.RemainingViews--
	val := time.Now()
	b := time.Time(secret.ExpiresAt)
	diff := b.Sub(val)

	if diff <= 0 {
		return
	}

	err3 := g.secretRepository.CreateNewSecret(g.redisClient,secret, diff)
	if err3 != nil {
		//log err
		return
	}
}